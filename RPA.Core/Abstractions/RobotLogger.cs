﻿using RPA.Abstractions;
using Serilog;
using System;

namespace RPA.Core.Abstractions
{
	public sealed class RobotLogger : IRobotLogger
	{
		private readonly ILogger Logger;

		public RobotLogger(ILogger logger)
		{
			Logger = logger ?? throw new ArgumentNullException(nameof(logger));
		}

		public void LogError(string error)
		{
			Logger.Error(error);
		}

		public void LogInformation(string info)
		{
			Logger.Information(info);
		}
	}
}
