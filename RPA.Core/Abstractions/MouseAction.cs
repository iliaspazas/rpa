﻿using RPA.Abstractions;
using System;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;

namespace RPA.Core.Abstractions
{
    public abstract class MouseAction : RobotAction
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        protected static extern IntPtr GetForegroundWindow();

        [DllImport("USER32.DLL")]
        protected static extern int GetWindowText(int hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("USER32.DLL")]
        protected static extern int GetWindowTextLength(int hWnd);

        protected struct Point
        {
            public int x;
            public int y;
        }

        protected struct Bounds
        {
            public int x;
            public int y;

            public static Bounds Empty = new Bounds() { x = 0, y = 0 };
            public bool IsEmpty()
            {
                return x == 0 && y == 0;
            }

            public static Bounds GetScreenBounds()
            {
                var scope = new ManagementScope();
                scope.Connect();

                var query = new ObjectQuery("SELECT * FROM Win32_VideoController");
                using (var searcher = new ManagementObjectSearcher(scope, query))
                {
                    var results = searcher.Get();
                    using (var e = results.GetEnumerator())
                        if (e.MoveNext())
                        {
                            return new Bounds()
                            {
                                x = Convert.ToInt32(e.Current.GetPropertyValue("CurrentHorizontalResolution")),
                                y = Convert.ToInt32(e.Current.GetPropertyValue("CurrentVerticalResolution"))
                            };
                        }
                }
                return Empty;
            }
        }

        protected struct Position
        {
            public double x;
            public double y;
        }

        protected Position ClientToScreen(Point p)
        {
            var bounds = Bounds.GetScreenBounds();
            if (!bounds.IsEmpty())
                return new Position()
                {
                    x = p.x * 65535.0 / bounds.x,
                    y = p.y * 65535.0 / bounds.y
                };
            return new Position()
            {
                x = 0,
                y = 0
            };
        }
    }
}