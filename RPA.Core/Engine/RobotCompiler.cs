﻿using RPA.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace RPA.Core.Engine
{
    public sealed class ScriptEmptyException : Exception
    {
        public ScriptEmptyException()
            : base("Script is empty.")
        { }
    }

    public sealed class MissingTokenException : Exception
    {
        public MissingTokenException(int line)
            : base($"Missing action token in line {line}.")
        { }
    }

    public sealed class UnknownTokenException : Exception
    {
        public UnknownTokenException(int line)
            : base($"Unknown action token in line {line}.")
        { }
    }

    public sealed class InvalidSyntaxException : Exception
    {
        public InvalidSyntaxException(int line)
            : base($"Invalid syntax in line {line}.")
        { }
    }

    public sealed class ParsingException : Exception
    {
        public ParsingException(int line, string error)
            : base($"Parsing error in line {line}. ({error})")
        { }
    }

    public sealed class AmbiguousSyntaxException : Exception
    {
        public AmbiguousSyntaxException(int line)
            : base($"Ambiguous syntax in line {line}.")
        { }
    }    

    public sealed class RobotCompiler
    {
        private readonly List<IRobotAction> _actions = null;
        private readonly string SourcePath;
        public RobotCompiler(string path)
        {
            SourcePath = path ?? throw new InvalidOperationException($"{nameof(path)} can't be empty.");
            _actions = new List<IRobotAction>();
        }

        public IEnumerable<IRobotAction> Actions => _actions;

        public void Refresh()
        {
            _actions.Clear();
            var modules = Directory.EnumerateFiles(SourcePath, "RPA.*.dll", SearchOption.TopDirectoryOnly).ToList();
            var type = typeof(IRobotAction);
            modules.ForEach((module) =>
            {
                var asm = Assembly.LoadFrom(module);
                var types = asm.GetTypes()
                    .Where(t => !t.IsAbstract && !t.IsInterface && t.GetInterfaces().Contains(type))
                    .ToList();
                types.ForEach((t) =>
                {
                    var action = Activator.CreateInstance(t) as IRobotAction;
                    if (action != null)
                        _actions.Add(action);
                });
            });
        }

        public RobotScript Compile(string source)
        {
            if (string.IsNullOrWhiteSpace(source))
                throw new ScriptEmptyException();

            var script = new RobotScript();

            var lines = source.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            int count = 0;
            foreach (var line in lines)
            {
                count++;
                if (!string.IsNullOrWhiteSpace(line))
                {
                    var token = line.Split(new[] { ' ' }).FirstOrDefault();
                    if (string.IsNullOrWhiteSpace(token))
                        throw new MissingTokenException(count);
                    var templateAction = Actions.FirstOrDefault(a => a.ActionToken == token);
                    if (templateAction == null)
                        throw new UnknownTokenException(count);
                    var re = new Regex(templateAction.Validator);
                    switch (re.Matches(line).Count)
                    {
                        case 0:
                            throw new InvalidSyntaxException(count);
                        case 1:
                            try
                            {
                                var action = templateAction.Clone();
                                action.Parse(line);
                                script.AddAction(action);
                                break;
                            }
                            catch (Exception e)
                            {
                                throw new ParsingException(count, e.Message);
                            }
                        default:
                            throw new AmbiguousSyntaxException(count);
                    }
                }
            }

            return script;
        }
    }
}
