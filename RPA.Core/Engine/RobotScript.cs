﻿using Newtonsoft.Json;
using RPA.Abstractions;
using RPA.Core.Abstractions;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace RPA.Core.Engine
{
	public sealed class RobotScript
	{
		public RobotScript()
		{
			_actions = new List<RobotAction>();
		}

		#region actions
		private readonly List<RobotAction> _actions = null;

		public IEnumerable<IRobotAction> Actions => _actions;

		public RobotScript AddAction(IRobotAction action)
		{
			_actions.Add(action as RobotAction);
			return this;
		}
		#endregion

		#region properties
		public string HotKey { get; set; }
		public string Name { get; set; }

		[JsonIgnore]
		public string Source
		{
			get
			{
				var sb = new StringBuilder();
				foreach (var action in Actions)
					sb.AppendLine(action.ActionCommand);
				return sb.ToString();
			}
		}
		#endregion

		#region serialization
		private static JsonSerializerSettings ss => new JsonSerializerSettings()
		{
			TypeNameHandling = TypeNameHandling.All,
			Formatting = Formatting.Indented
		};

		public void SaveToFile(string path)
		{
			File.WriteAllText(path, Serialize());
		}

		public static RobotScript LoadFromFile(string path)
		{
			var source = File.ReadAllText(path);
			return Deserialize(source);
		}

		public string Serialize()
		{
			return Serialize(this);
		}

		public string AsBase64()
		{
			return Convert.ToBase64String(Encoding.UTF8.GetBytes(Serialize()));
		}

		public static RobotScript FromBase64(string data)
		{
			var source = Encoding.UTF8.GetString(Convert.FromBase64String(data));
			return Deserialize(source);
		}

		public static string Serialize(RobotScript source)
		{
			return JsonConvert.SerializeObject(source, ss);
		}

		public static RobotScript Deserialize(string source)
		{
			return JsonConvert.DeserializeObject<RobotScript>(source, ss);
		}
		#endregion

		#region exec
		public void Execute(ILogger logger)
		{
			foreach (var action in Actions)
				try
				{
					action.Logger = new RobotLogger(logger);
					action.Execute();
				}
				catch (Exception e)
				{
					throw new RobotActionException(action.Description, "Action failed.", e);
				}
				finally
				{
					action.Logger = null;
				}
		}
		#endregion
	}
}
