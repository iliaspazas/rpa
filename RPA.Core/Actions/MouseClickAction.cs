﻿using RPA.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindowsInput;

namespace RPA.Core.Actions
{
	public sealed class MouseClickAction : MouseAction
	{
        public const string TOKEN = "mouse-click";

        public ClickKind Kind { get; set; }

		public MouseClickAction()
			: base()
		{
			Description = "Mouse Click";
			ActionToken = TOKEN;
			Validator = $"({ActionToken}) (Single$|Double$)";
		}

		public MouseClickAction(ClickKind kind)
			: this()
		{
			Kind = kind;
		}

		protected override string ComposeCommand()
		{
			return $"{ActionToken} {Kind.ToString()}";
		}

		protected override string ComposeTemplate()
		{
			return $"{ActionToken} [{ClickKind.Single.ToString()}|{ClickKind.Double.ToString()}]";
		}

		protected override void DoParse(IEnumerable<string> arguments)
		{
			Kind = (ClickKind)Enum.Parse(typeof(ClickKind), arguments.First());
		}

		protected override void DoExecute()
		{
			IntPtr handle = GetForegroundWindow();

			int length = GetWindowTextLength(handle.ToInt32());
			var builder = new StringBuilder(length);
			GetWindowText(handle.ToInt32(), builder, length + 1);
			LogInformation($"Window titled \"{builder.ToString()}\" found.");

			switch (Kind)
			{
				case ClickKind.Single:
					LogInformation($"Performing single click.");
                    new InputSimulator().Mouse.LeftButtonClick();
					break;
				case ClickKind.Double:
					LogInformation($"Performing double click.");
                    new InputSimulator().Mouse.LeftButtonDoubleClick();
					break;
			}
		}
	}
}
