﻿using RPA.Abstractions;
using System.Collections.Generic;
using System.Linq;
using WindowsInput;

namespace RPA.Core.Actions
{
	public sealed class SendTextAction : RobotAction
	{
        public const string TOKEN = "send-keys";

        public string Text { get; set; }

		public SendTextAction()
			: base()
		{
			Description = "Send Text";
			ActionToken = TOKEN;
			Validator = $"({ActionToken}) (\".*\")";
		}

		public SendTextAction(string text)
			: this()
		{
			Text = text;
		}

		protected override string ComposeCommand()
		{
			return $"{ActionToken} {Text}";
		}

		protected override string ComposeTemplate()
		{
			return $"{ActionToken} \"[free text]\"";
		}

		protected override void DoParse(IEnumerable<string> arguments)
		{
			Text = arguments.First();
		}

		protected override void DoExecute()
		{
            var input = Text?.Trim(new[] { '"' });
            LogInformation($"Sending keyboard input '{input}' started.");
            new InputSimulator().Keyboard.TextEntry(input);
            LogInformation($"Sending keyboard input ended.");
		}
	}
}
