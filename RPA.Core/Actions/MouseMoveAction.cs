﻿using RPA.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindowsInput;

namespace RPA.Core.Actions
{
    public sealed class MouseMoveAction : MouseAction
    {
        public const string TOKEN = "mouse-move";

        public int X { get; set; }
        public int Y { get; set; }

        public MouseMoveAction()
            : base()
        {
            Description = "Mouse Move";
            ActionToken = TOKEN;
            Validator = $@"({ActionToken}) (\d+), (\d+)";
        }

        public MouseMoveAction(int x, int y)
            : this()
        {
            X = x;
            Y = y;
        }

        protected override string ComposeCommand()
        {
            return $"{ActionToken} {X}, {Y}";
        }

        protected override string ComposeTemplate()
        {
            return $"{ActionToken} [int X coord], [int Y coord]";
        }

        protected override void DoParse(IEnumerable<string> arguments)
        {
            X = int.Parse(arguments.ElementAt(0));
            Y = int.Parse(arguments.ElementAt(1));
        }

        protected override void DoExecute()
        {
            IntPtr handle = GetForegroundWindow();

            int length = GetWindowTextLength(handle.ToInt32());
            var builder = new StringBuilder(length);
            GetWindowText(handle.ToInt32(), builder, length + 1);
            LogInformation($"Window titled \"{builder.ToString()}\" found.");

            Point p = new Point
            {
                x = X,
                y = Y
            };

            LogInformation($"Input position [{p.x}, {p.y}]");
            var pos = ClientToScreen(p);
            LogInformation($"Position to [{pos.x}, {pos.y}]");

            new InputSimulator().Mouse.MoveMouseToPositionOnVirtualDesktop(pos.x, pos.y);
        }
    }
}
