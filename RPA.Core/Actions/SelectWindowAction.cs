﻿using RPA.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace RPA.Core.Actions
{
	public sealed class SelectWindowAction : RobotAction
	{
		[DllImport("User32.dll")]
		private static extern int FindWindow(string ClassName, string WindowName);

		[DllImport("User32.dll")]
		private static extern IntPtr SetForegroundWindow(int hWnd);

		[DllImport("user32.dll")]
		private static extern int ShowWindow(int hWnd, uint Msg);

		private const uint SW_RESTORE = 0x09;

        public const string TOKEN = "select-window";

        public string Window { get; set; }

		public SelectWindowAction()
			: base()
		{
			Description = "Select Window";
			ActionToken = TOKEN;
			Validator = $"({ActionToken}) (\".*\")";
		}

		public SelectWindowAction(string window)
			: this()
		{
			Window = window;
		}

		protected override string ComposeCommand()
		{
			return $"{ActionToken} {Window}";
		}

		protected override string ComposeTemplate()
		{
			return $"{ActionToken} \"[Text Window Title]\"";
		}

		protected override void DoParse(IEnumerable<string> arguments)
		{
			Window = arguments.First();
		}

		protected override void DoExecute()
		{
			if (string.IsNullOrWhiteSpace(Window))
				throw new InvalidOperationException("Window title not defined.");

			var title = Window.Trim(new[] { '"' });
			int hWnd = FindWindow(null, title);
			if (hWnd > 0)
			{
				LogInformation($"Window titled \"{title}\" found.");
				ShowWindow(hWnd, SW_RESTORE);
				SetForegroundWindow(hWnd);
				LogInformation($"Window titled \"{title}\" set to foreground.");
			}
			else
				throw new Exception("Window not found.");
		}
	}
}
