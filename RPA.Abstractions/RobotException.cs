﻿using System;

namespace RPA.Abstractions
{
	public sealed class RobotActionException : Exception
	{
		public string ActionName { get; set; }

		public RobotActionException(string actionName, string message)
			: base(message)
		{
			ActionName = actionName;
		}

		public RobotActionException(string actionName, string message, Exception innerException)
			: base(message, innerException)
		{
			ActionName = actionName;
		}
	}
}
