﻿namespace RPA.Abstractions
{
	public static class RPAConstants
	{
		public const string URL = @"http://localhost:8088/";
		public const string HUB_NAME = "RPAHub";

		public const string MSG_REGISTER = "_REGISTER_";
		public const string MSG_UN_REGISTER = "_UN_REGISTER_";
		public const string MSG_HOT_KEY = "_HOT_KEY_";
		public const string MSG_SEND = "_SEND_";
	}
}
