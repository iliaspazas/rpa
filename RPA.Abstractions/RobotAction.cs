﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace RPA.Abstractions
{
	public abstract class RobotAction : IRobotAction
	{
		public RobotAction()
		{
		}

		public string Description { get; set; }
		public string ActionToken { get; set; }
		public string Validator { get; set; }

		public string ActionCommand => ComposeCommand();
		public string ActionTemplate => ComposeTemplate();

		[JsonIgnore]
		public IRobotLogger Logger { get; set; }

		protected abstract string ComposeCommand();
		protected abstract string ComposeTemplate();

		public override string ToString()
		{
			return Description ?? base.ToString();
		}

		public void Parse(string line)
		{
			var re = new Regex(Validator);
			var parts = re.Split(line).Where(o => !string.IsNullOrWhiteSpace(o)).ToList();
			if (parts.Count < 2)
				throw new InvalidOperationException("Invalid number of expression blocks.");
			if (ActionToken != parts.First())
				throw new InvalidOperationException($"Invalid action token {parts.First()}.");
			parts.RemoveAt(0);
			DoParse(parts);
		}

		protected abstract void DoParse(IEnumerable<string> arguments);

		public IRobotAction Clone()
		{
			return MemberwiseClone() as IRobotAction;
		}

		protected abstract void DoExecute();

		public void Execute()
		{
			DoExecute();
		}

		protected void LogInformation(string info)
		{
			Logger?.LogInformation(info);
		}

		protected void LogError(string error)
		{
			Logger?.LogError(error);
		}
	}
}
