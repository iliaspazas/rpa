﻿namespace RPA.Abstractions
{
	public interface IRobotAction
	{
		string Description { get; set; }
		string ActionToken { get; set; }
		string ActionTemplate { get; }
		string ActionCommand { get; }
		string Validator { get; set; }

		void Parse(string line);
		IRobotAction Clone();
		void Execute();

		IRobotLogger Logger { get; set; }
	}
}
