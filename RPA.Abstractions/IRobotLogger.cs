﻿namespace RPA.Abstractions
{
	public interface IRobotLogger
	{
		void LogInformation(string info);
		void LogError(string error);
	}
}
