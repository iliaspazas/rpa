﻿using Microsoft.AspNet.SignalR.Client;
using RPA.Abstractions;
using RPA.Core.Engine;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace RPA.Agent
{
    internal class Program
    {
        private static ManualResetEvent evt = new ManualResetEvent(false);
        private static CancellationTokenSource cts = new CancellationTokenSource();
        private static string clientID;
        private static int reTries = 0;
        private static IHubProxy _hub;

        private static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.ColoredConsole()
                .CreateLogger();

            clientID = DateTime.Now.Ticks.ToString();
            Log.Information($"Client ID: {clientID}");

            Console.Title += $" - [{clientID}]";

            InitChannel();

            Log.Warning("Press <ENTER> to terminate.");

            ProcessKeys();

            if (!cts.Token.IsCancellationRequested)
                cts.Cancel();

            evt.WaitOne();
        }

        private static void ProcessKeys()
        {
            ConsoleKeyInfo input;
            while ((input = Console.ReadKey()) != null)
            {
                bool ctrl = (input.Modifiers & ConsoleModifiers.Control) == ConsoleModifiers.Control;
                bool alt = (input.Modifiers & ConsoleModifiers.Alt) == ConsoleModifiers.Alt;
                if (input.Key == ConsoleKey.Enter)
                {
                    cts.Cancel();
                    break;
                }

                var key = string.Empty;
                switch (input.Key)
                {
                    case ConsoleKey.D0:
                        key = "0";
                        break;
                    case ConsoleKey.D1:
                        key = "1";
                        break;
                    case ConsoleKey.D2:
                        key = "2";
                        break;
                    case ConsoleKey.D3:
                        key = "3";
                        break;
                    case ConsoleKey.D4:
                        key = "4";
                        break;
                    case ConsoleKey.D5:
                        key = "5";
                        break;
                    case ConsoleKey.D6:
                        key = "6";
                        break;
                    case ConsoleKey.D7:
                        key = "7";
                        break;
                    case ConsoleKey.D8:
                        key = "8";
                        break;
                    case ConsoleKey.D9:
                        key = "9";
                        break;
                    case ConsoleKey.F1:
                        key = "F1";
                        break;
                    case ConsoleKey.F2:
                        key = "F2";
                        break;
                    case ConsoleKey.F3:
                        key = "F3";
                        break;
                    case ConsoleKey.F4:
                        key = "F4";
                        break;
                    case ConsoleKey.F5:
                        key = "F5";
                        break;
                    case ConsoleKey.F6:
                        key = "F6";
                        break;
                    case ConsoleKey.F7:
                        key = "F7";
                        break;
                    case ConsoleKey.F8:
                        key = "F8";
                        break;
                    case ConsoleKey.F9:
                        key = "F9";
                        break;
                    case ConsoleKey.F10:
                        key = "F10";
                        break;
                    case ConsoleKey.F11:
                        key = "F11";
                        break;
                    case ConsoleKey.F12:
                        key = "F12";
                        break;
                }

                if (!string.IsNullOrWhiteSpace(key))
                {
                    string hotKey = string.Empty;
                    if (ctrl && alt)
                        hotKey = $"CTRL + ALT + {key}";
                    else if (ctrl)
                        hotKey = $"CTRL + {key}";
                    else if (alt)
                        hotKey = $"ALT + {key}";

                    if (!string.IsNullOrWhiteSpace(hotKey))
                        SendKey(hotKey);
                }
            }
        }

        private static void SendKey(string hotKey)
        {
            Task.Run(async () =>
            {
                try
                {
                    await _hub.Invoke("SendHotKey", clientID, hotKey);
                }
                catch (Exception e)
                {
                    Log.Error(e.ToString());
                }
            });
        }

        private static void InitChannel()
        {
            Task.Run(async () =>
            {
                using (var connection = new HubConnection(RPAConstants.URL))
                {
                    _hub = connection.CreateHubProxy(RPAConstants.HUB_NAME);

                    connection.StateChanged += (StateChange sc) => Task.Run(async () => await onStateChanged(sc, clientID));
                    connection.Closed += () => Task.Run(() => onClosed());
                    connection.Error += (Exception e) => Task.Run(() => onError(e));
                    connection.Received += (string data) => Task.Run(() => onReceived(data));

                    await connection.Start();

                    _hub.On("ClientIsRegistered", x => Log.Information(x));
                    _hub.On("ClientIsDeRegistered", x => Log.Information(x));
                    _hub.On("ExecuteScript", x => onExecuteScript(x));

                    while (!cts.Token.IsCancellationRequested)
                        await Task.Delay(1);

                    await _hub.Invoke("DeRegister", clientID);
                }
            });
        }

        private static void onExecuteScript(dynamic data)
        {
            RobotScript script = RobotScript.FromBase64(data.ToString());
            Task.Run(async () =>
            {
                try
                {
                    script.Execute(Log.Logger);
                    Log.Information($"Script: {script.Name} is successful.");
                    await _hub.Invoke("ReportSuccess", script.Name);
                }
                catch (RobotActionException rae)
                {
                    Log.Error($"Script: {script.Name} failed.{Environment.NewLine}{rae.ToString()}");
                    await _hub.Invoke("ReportError", script.Name, rae.ActionName, rae.ToString());
                }
                catch (Exception e)
                {
                    Log.Error($"Script: {script.Name} failed.{Environment.NewLine}{e.ToString()}");
                    await _hub.Invoke("ReportError", script.Name, string.Empty, e.ToString());
                }
            });
        }

        private static void onReceived(string data)
        {
            Log.Information($"Received: {data}");
        }

        private static void onError(Exception obj)
        {
            Log.Error($"Error: {obj.Message}");
        }

        private static async Task onStateChanged(StateChange sc, string clientID)
        {
            Log.Information($"State: {sc.NewState}");
            switch (sc.NewState)
            {
                case ConnectionState.Connected:
                    await _hub.Invoke("Register", clientID);
                    break;
                case ConnectionState.Connecting:
                    break;
                case ConnectionState.Disconnected:
                    await _hub.Invoke("DeRegister", clientID);
                    break;
                case ConnectionState.Reconnecting:
                    break;
            }
        }

        private static void onClosed()
        {
            Log.Information("Closing...");
            reTries++;

            if (reTries < 3 && !cts.Token.IsCancellationRequested)
            {
                Thread.Sleep(5000);
                InitChannel();
            }
            else
            {
                if (!cts.Token.IsCancellationRequested)
                    cts.Cancel();
                evt.Set();
            }
        }
    }
}