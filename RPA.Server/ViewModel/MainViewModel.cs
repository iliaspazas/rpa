using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.AspNet.SignalR;
using RPA.Abstractions;
using RPA.Core.Engine;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace RPA.Server.ViewModel
{
	public class MainViewModel : ViewModelBase, ILogEventSink
	{
		#region commons
		public string AppTitle { get; } = "RPA Server";

		public MainViewModel()
		{
			if (IsInDesignMode)
			{
			}
			else
			{
				Directory.CreateDirectory(ScriptsPath);
				RegisterMessaging();
			}
		}

		private void RegisterMessaging()
		{
			MessengerInstance.Register<GenericMessage<string>>(this, RPAConstants.MSG_REGISTER, (msg) =>
			{
				Application.Current.Dispatcher.BeginInvoke(new Action(() =>
				{
					Clients.Add(msg.Content);
				}));
			});
			MessengerInstance.Register<GenericMessage<string>>(this, RPAConstants.MSG_UN_REGISTER, (msg) =>
			{
				Application.Current.Dispatcher.BeginInvoke(new Action(() =>
				{
					Clients.Remove(msg.Content);
				}));
			});
			MessengerInstance.Register<GenericMessage<KeyValuePair<string, string>>>(this, RPAConstants.MSG_HOT_KEY, (msg) =>
			{
				Application.Current.Dispatcher.BeginInvoke(new Action(() =>
				{
					FindScriptAndSend(msg.Content.Key, msg.Content.Value);
				}));
			});
		}

		private string AppPath => Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
		private string ScriptsPath => Path.Combine(AppPath, "Scripts");
		private string ScriptFullPath => GetScriptFullPath(ScriptName);

		private string GetScriptFullPath(string name)
		{
			return Path.ChangeExtension(Path.Combine(ScriptsPath, name), ".rpa");
		}

		private RobotCompiler _compiler;
		private RobotCompiler Compiler
		{
			get
			{
				if (_compiler == null)
				{
					_compiler = new RobotCompiler(AppPath);
					_compiler.Refresh();
				}
				return _compiler;
			}
		}
		#endregion

		#region clients
		private ObservableCollection<string> _clients;
		public ObservableCollection<string> Clients
		{
			get
			{
				return _clients ?? (_clients = new ObservableCollection<string>());
			}
		}

		private int _selectedClient = -1;
		public int SelectedClient
		{
			get
			{
				return _selectedClient;
			}
			set
			{
				_selectedClient = value;
				RaisePropertyChanged(() => SelectedClient);
				SendCommand.RaiseCanExecuteChanged();
			}
		}

		private string FindClientByID(string ID)
		{
			return Clients.FirstOrDefault(c => !string.IsNullOrWhiteSpace(c.Split(new string[] { " - " }, StringSplitOptions.None).FirstOrDefault(o => o.Contains(ID))));
		}
		#endregion

		#region scripts
		private ObservableCollection<string> _scripts;
		public ObservableCollection<string> Scripts
		{
			get
			{
				if (_scripts == null)
				{
					_scripts = new ObservableCollection<string>();
					Directory.EnumerateFiles(ScriptsPath, "*.rpa", SearchOption.TopDirectoryOnly)
						.ToList()
						.ForEach((script) => _scripts.Add(Path.GetFileName(script)));
				}
				return _scripts;
			}
		}

		private int _selectedScript = -1;
		public int SelectedScript
		{
			get
			{
				return _selectedScript;
			}
			set
			{
				_selectedScript = value;
				RaisePropertyChanged(() => SelectedScript);
				SendCommand.RaiseCanExecuteChanged();
				SaveCommand.RaiseCanExecuteChanged();
				if (_selectedScript >= 0)
				{
					ScriptName = Scripts[_selectedScript];
					var script = RobotScript.LoadFromFile(ScriptFullPath);
					ScriptSource = script.Source;
					ScriptHotKey = script.HotKey;
				}
				else
				{
					ScriptName = string.Empty;
					ScriptSource = string.Empty;
				}
			}
		}

		private void ReloadScripts()
		{
			_scripts = null;
			RaisePropertyChanged(() => Scripts);
		}

		private RelayCommand _sendCommand;
		public RelayCommand SendCommand
		{
			get
			{
				return _sendCommand
				  ?? (_sendCommand = new RelayCommand(
					() =>
					{
						SendScript(Clients[SelectedClient], ScriptFullPath);
					},
					() => SelectedScript >= 0 && SelectedClient >= 0));
			}
		}

		private RelayCommand _reloadScriptsCommand;
		public RelayCommand ReloadScriptsCommand
		{
			get
			{
				return _reloadScriptsCommand
				  ?? (_reloadScriptsCommand = new RelayCommand(
					() =>
					{
						ReloadScripts();
					}));
			}
		}

		private void SendScript(string client, string path)
		{
			var cID = client.Split(new string[] { " - " }, StringSplitOptions.None).First().Trim();
			var ctx = GlobalHost.ConnectionManager.GetHubContext(RPAConstants.HUB_NAME);
			var script = RobotScript.LoadFromFile(path);
			ctx.Clients.Client(cID).ExecuteScript(script.AsBase64());
		}

		private void FindScriptAndSend(string clientID, string key)
		{
			var client = FindClientByID(clientID);
			if (string.IsNullOrWhiteSpace(client))
			{
				Log.Error($"Client [{clientID}] not found.");
				return;
			}

			string path = string.Empty;
			foreach (var s in Scripts)
			{
				path = GetScriptFullPath(s);
				var script = RobotScript.LoadFromFile(path);
				if (script.HotKey == key)
					break;
				path = string.Empty;
			}

			if (!string.IsNullOrWhiteSpace(path))
				SendScript(client, path);
			else
				Log.Warning($"Script for hotkey [{key}] not found.");
		}
		#endregion

		#region logging
		public void Emit(LogEvent logEvent)
		{
			Application.Current.Dispatcher.BeginInvoke(new Action(() =>
			{
				Logs.Insert(0, $"[{logEvent.Level.ToString()}], {logEvent.Timestamp.ToString("dd/MM/yyyy HH:mm:ss")} {logEvent.Exception?.Message ?? logEvent.MessageTemplate?.Text}");
				RaisePropertyChanged(() => Logs);
			}));
		}

		private ObservableCollection<string> _logs;
		public ObservableCollection<string> Logs
		{
			get
			{
				return _logs ?? (_logs = new ObservableCollection<string>());
			}
		}
		#endregion

		#region actions
		private ObservableCollection<IRobotAction> _actions;
		public ObservableCollection<IRobotAction> Actions
		{
			get
			{
				if (_actions == null)
				{
					_actions = new ObservableCollection<IRobotAction>(Compiler.Actions);
				}
				return _actions;
			}
		}

		private int _selectedAction = -1;
		public int SelectedAction
		{
			get
			{
				return _selectedAction;
			}
			set
			{
				_selectedAction = value;
				RaisePropertyChanged(() => SelectedAction);
				AddActionCommand.RaiseCanExecuteChanged();
			}
		}

		private RelayCommand _addActionCommand;
		public RelayCommand AddActionCommand
		{
			get
			{
				return _addActionCommand
				  ?? (_addActionCommand = new RelayCommand(
					() =>
					{
						var action = Actions[SelectedAction];
						if (string.IsNullOrWhiteSpace(ScriptSource))
							ScriptSource = action.ActionTemplate;
						else
							ScriptSource = $"{ScriptSource}{Environment.NewLine}{action.ActionTemplate}";
					},
					() => SelectedAction >= 0));
			}
		}

		private RelayCommand _refreshActionsCommand;
		public RelayCommand RefreshActionsCommand
		{
			get
			{
				return _refreshActionsCommand
				  ?? (_refreshActionsCommand = new RelayCommand(
					() =>
					{
						Compiler.Refresh();
						_actions = null;
						RaisePropertyChanged(() => Actions);
					}));
			}
		}
		#endregion

		#region editor
		private string _scriptSource;
		public string ScriptSource
		{
			get
			{
				return _scriptSource;
			}
			set
			{
				_scriptSource = value;
				RaisePropertyChanged(() => ScriptSource);
			}
		}

		private string _scriptName;
		public string ScriptName
		{
			get
			{
				return _scriptName;
			}
			set
			{
				_scriptName = value;
				RaisePropertyChanged(() => ScriptName);
				SaveCommand.RaiseCanExecuteChanged();
			}
		}

		private string _scriptHotKey;
		public string ScriptHotKey
		{
			get
			{
				return _scriptHotKey;
			}
			set
			{
				_scriptHotKey = value;
				RaisePropertyChanged(() => ScriptHotKey);
			}
		}

		private RelayCommand _saveCommand;
		public RelayCommand SaveCommand
		{
			get
			{
				return _saveCommand
				  ?? (_saveCommand = new RelayCommand(
					() =>
					{
						var script = Compiler.Compile(ScriptSource);
						script.HotKey = ScriptHotKey?.ToUpper();
						script.Name = ScriptName;
						script.SaveToFile(ScriptFullPath);
						ScriptSource = script.Source;
						ReloadScripts();
					},
					() => SelectedScript >= 0 || !string.IsNullOrWhiteSpace(ScriptName)));
			}
		}
		#endregion
	}
}