﻿using GalaSoft.MvvmLight.Messaging;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using RPA.Abstractions;
using Serilog;
using System.Collections.Generic;

namespace RPA.Server.Hubs
{
	[HubName(RPAConstants.HUB_NAME)]
	public sealed class RPAHub : Hub
	{
		public void Register(string clientID)
		{
			var msg = $"Registration for client: {clientID}";
			Log.Information(msg);

			Messenger.Default.Send(new GenericMessage<string>($"{Context.ConnectionId} - {clientID}"), RPAConstants.MSG_REGISTER);

			Clients.Caller.ClientIsRegistered(msg);
		}

		public void DeRegister(string clientID)
		{
			var msg = $"De-registration for client: {clientID}";
			Log.Information(msg);

			Messenger.Default.Send(new GenericMessage<string>($"{Context.ConnectionId} - {clientID}"), RPAConstants.MSG_UN_REGISTER);

			Clients.Caller.ClientIsDeRegistered(msg);
		}

		public void SendHotKey(string clientID, string key)
		{
			var msg = $"Hotkey '{key}' received.";
			Log.Information(msg);
			var kvp = new KeyValuePair<string, string>(clientID, key);
			Messenger.Default.Send(new GenericMessage<KeyValuePair<string, string>>(kvp), RPAConstants.MSG_HOT_KEY);
		}

		public void ReportError(string scriptName, string errorneousAction, string error)
		{
			var msg = $"Action '{errorneousAction}' in script: {scriptName}, failed. Error: {error}";
			Log.Warning(msg);
		}

		public void ReportSuccess(string scriptName)
		{
			var msg = $"Script '{scriptName}' has been completed.";
			Log.Information(msg);
		}
	}
}
