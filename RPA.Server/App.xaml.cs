﻿using Microsoft.Owin.Hosting;
using RPA.Abstractions;
using RPA.Server.ViewModel;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace RPA.Server
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private readonly CancellationTokenSource cts = new CancellationTokenSource();

		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);

			Serilog.Debugging.SelfLog.Enable(msg => System.Diagnostics.Debug.WriteLine(msg));

			var locator = Current.Resources["Locator"] as ViewModelLocator;

			Log.Logger = new LoggerConfiguration()
				.WriteTo.RollingFile(@"Logs\log-{Date}.txt")
				.WriteTo.Sink(locator.Main)
				.CreateLogger();

			RegisterGlobalExceptionHandling(Log.Logger);

			Task.Run(async () =>
			{
				using (WebApp.Start<StartUp>(RPAConstants.URL))
					while (!cts.Token.IsCancellationRequested)
						await Task.Delay(100);
			});
		}

		protected override void OnExit(ExitEventArgs e)
		{
			cts.Cancel();
			base.OnExit(e);
		}

		private void RegisterGlobalExceptionHandling(ILogger log)
		{
			AppDomain.CurrentDomain.UnhandledException +=
				(sender, args) => CurrentDomainOnUnhandledException(args, log);

			Current.DispatcherUnhandledException +=
				(sender, args) => CurrentOnDispatcherUnhandledException(args, log);

			TaskScheduler.UnobservedTaskException +=
				(sender, args) => TaskSchedulerOnUnobservedTaskException(args, log);
		}

		private static void TaskSchedulerOnUnobservedTaskException(UnobservedTaskExceptionEventArgs args, ILogger log)
		{
			log.Error(args.Exception.ToString());
			args.SetObserved();
		}

		private static void CurrentOnDispatcherUnhandledException(DispatcherUnhandledExceptionEventArgs args, ILogger log)
		{
			log.Error(args.Exception.ToString());
			args.Handled = true;
		}

		private static void CurrentDomainOnUnhandledException(UnhandledExceptionEventArgs args, ILogger log)
		{
			var exception = args.ExceptionObject as Exception;
			var terminatingMessage = args.IsTerminating ? " The application is terminating." : string.Empty;
			var exceptionMessage = exception?.Message ?? "An unmanaged exception occured.";
			var message = string.Concat(exceptionMessage, terminatingMessage);
			log.Error(exception, message);
		}
	}
}
