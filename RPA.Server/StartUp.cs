﻿using Microsoft.Owin.Cors;
using Owin;

namespace RPA.Server
{
	internal class StartUp
	{
		public void Configuration(IAppBuilder app)
		{
			app.UseCors(CorsOptions.AllowAll);
			app.MapSignalR();
		}
	}
}
