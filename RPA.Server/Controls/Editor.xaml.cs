﻿using System.Windows.Controls;

namespace RPA.Server
{
    /// <summary>
    /// Interaction logic for Editor.xaml
    /// </summary>
    public partial class Editor : UserControl
    {
        public Editor()
        {
            InitializeComponent();
        }

        private void TextBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            string key = string.Empty;
            switch (e.Key)
            {
                case System.Windows.Input.Key.D0:
                    key = "0";
                    break;
                case System.Windows.Input.Key.D1:
                    key = "1";
                    break;
                case System.Windows.Input.Key.D2:
                    key = "2";
                    break;
                case System.Windows.Input.Key.D3:
                    key = "3";
                    break;
                case System.Windows.Input.Key.D4:
                    key = "4";
                    break;
                case System.Windows.Input.Key.D5:
                    key = "5";
                    break;
                case System.Windows.Input.Key.D6:
                    key = "6";
                    break;
                case System.Windows.Input.Key.D7:
                    key = "7";
                    break;
                case System.Windows.Input.Key.D8:
                    key = "8";
                    break;
                case System.Windows.Input.Key.D9:
                    key = "9";
                    break;
                case System.Windows.Input.Key.F1:
                    key = "F1";
                    break;
                case System.Windows.Input.Key.F2:
                    key = "F2";
                    break;
                case System.Windows.Input.Key.F3:
                    key = "F3";
                    break;
                case System.Windows.Input.Key.F4:
                    key = "F4";
                    break;
                case System.Windows.Input.Key.F5:
                    key = "F5";
                    break;
                case System.Windows.Input.Key.F6:
                    key = "F6";
                    break;
                case System.Windows.Input.Key.F7:
                    key = "F7";
                    break;
                case System.Windows.Input.Key.F8:
                    key = "F8";
                    break;
                case System.Windows.Input.Key.F9:
                    key = "F9";
                    break;
                case System.Windows.Input.Key.F10:
                    key = "F10";
                    break;
                case System.Windows.Input.Key.F11:
                    key = "F11";
                    break;
                case System.Windows.Input.Key.F12:
                    key = "F12";
                    break;
                case System.Windows.Input.Key.LeftCtrl:
                case System.Windows.Input.Key.RightCtrl:
                    tbHotKey.Text = string.Empty;
                    key = "CTRL";
                    break;
                case System.Windows.Input.Key.LeftAlt:
                case System.Windows.Input.Key.RightAlt:
                    tbHotKey.Text = string.Empty;
                    key = "CTRL + ALT";
                    break;
            }

            if (!string.IsNullOrWhiteSpace(key))
            {
                if (!string.IsNullOrWhiteSpace(tbHotKey.Text))
                {
                    if (tbHotKey.Text.Contains("+"))
                        tbHotKey.Text = string.Empty;

                    if (tbHotKey.Text.StartsWith("CTRL"))
                        tbHotKey.Text += $" + {key}";
                    else
                        tbHotKey.Text = string.Empty;
                }
                else if (key.StartsWith("CTRL"))
                    tbHotKey.Text = key;
                else
                    tbHotKey.Text = string.Empty;
            }
            else
                tbHotKey.Text = string.Empty;
            e.Handled = true;
        }
    }
}
