﻿using System.Linq;
using System.IO;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPA.Core.Engine;
using System;

namespace RPA.Tests
{
    [TestClass]
    public class RobotCompilerTests
    {
        private static string ActionModulePath;

        [ClassInitialize]
        public static void TestFixtureSetup(TestContext context)
        {
            ActionModulePath = Path.GetDirectoryName(Assembly.GetCallingAssembly().Location);
        }

        [TestMethod]
        public void Compiler_Has_Actions()
        {
            var c = new RobotCompiler(ActionModulePath);
            c.Refresh();
            Assert.IsTrue(c.Actions.Count() != 0);
        }

        [TestMethod]
        public void Compiler_Has_Specific_Action()
        {
            var c = new RobotCompiler(ActionModulePath);
            c.Refresh();
            var token = new CustomTestAction().ActionToken;
            Assert.IsNotNull(c.Actions.FirstOrDefault(a => a.ActionToken == token));
        }

        [TestMethod]
        public void Compiler_Compiles_Exactly_One_Action()
        {
            var c = new RobotCompiler(ActionModulePath);
            c.Refresh();
            var script = c.Compile($"{CustomTestAction.TOKEN} 1, \"xyz\", -1");
            Assert.IsTrue(script.Actions.Count() == 1);
        }

        [TestMethod]
        public void Compiler_Compiles_To_Matching_Action()
        {
            var action = new CustomTestAction(1, "xyz", -1);
            var c = new RobotCompiler(ActionModulePath);
            c.Refresh();
            var script = c.Compile($"{CustomTestAction.TOKEN} 1, xyz, -1");
            Assert.IsTrue(script.Actions.First().ActionCommand == action.ActionCommand);
        }

        [TestMethod]
        public void Compiler_Throws_Error_On_Null_Script()
        {
            var c = new RobotCompiler(ActionModulePath);
            c.Refresh();
            Assert.ThrowsException<ScriptEmptyException>(() =>
            {
                c.Compile(null);
            });
        }

        [TestMethod]
        public void Compiler_Throws_Error_On_Empty_Script()
        {
            var c = new RobotCompiler(ActionModulePath);
            c.Refresh();
            Assert.ThrowsException<ScriptEmptyException>(() =>
            {
                c.Compile(null);
            });
        }

        [TestMethod]
        public void Compiler_Throws_Error_On_Unknown_Token()
        {
            var c = new RobotCompiler(ActionModulePath);
            c.Refresh();
            Assert.ThrowsException<UnknownTokenException>(() =>
            {
                c.Compile($"TOKEN 1, xyz, -1");
            });
        }

        [TestMethod]
        public void Compiler_Throws_Error_On_Missing_Token()
        {
            var c = new RobotCompiler(ActionModulePath);
            c.Refresh();
            Assert.ThrowsException<MissingTokenException>(() =>
            {
                c.Compile($" 1, xyz, -1");
            });
        }

        [TestMethod]
        public void Compiler_Throws_Error_On_Invalid_Syntax()
        {
            var c = new RobotCompiler(ActionModulePath);
            c.Refresh();
            Assert.ThrowsException<InvalidSyntaxException>(() =>
            {
                c.Compile($"{CustomTestAction.TOKEN} -1, xyz, -1");
            });
        }

        [TestMethod]
        public void Compiler_Throws_Error_On_Invalid_Syntax_2()
        {
            var c = new RobotCompiler(ActionModulePath);
            c.Refresh();
            Assert.ThrowsException<InvalidSyntaxException>(() =>
            {
                c.Compile($"{CustomTestAction.TOKEN} 1, xyz, 1");
            });
        }

        [TestMethod]
        public void Compiler_Throws_Error_On_Invalid_Syntax_3()
        {
            var c = new RobotCompiler(ActionModulePath);
            c.Refresh();
            Assert.ThrowsException<InvalidSyntaxException>(() =>
            {
                c.Compile($"{CustomTestAction.TOKEN} 1, 1");
            });
        }

        [TestMethod]
        public void Compiler_Throws_Error_On_Invalid_Syntax_4()
        {
            var c = new RobotCompiler(ActionModulePath);
            c.Refresh();
            Assert.ThrowsException<InvalidSyntaxException>(() =>
            {
                c.Compile($"{CustomTestAction.TOKEN} 1, xyz, 1, 2");
            });
        }
    }
}