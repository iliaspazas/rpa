﻿using RPA.Abstractions;
using System;
using System.Linq;
using System.Collections.Generic;

namespace RPA.Tests
{
    public sealed class CustomTestAction : RobotAction
    {
        public const string TOKEN = "custom-test";

        public int Arg1 { get; set; }
        public string Arg2 { get; set; }
        public int Arg3 { get; set; }

        public CustomTestAction()
            : base()
        {
            Description = "Custom Test Action";
            ActionToken = TOKEN;
            Validator = $"({ActionToken}) (\\d+), (\\S+), (-\\d+)";
        }

        public CustomTestAction(int arg1, string arg2, int arg3)
            : this()
        {
            Arg1 = arg1;
            Arg2 = arg2;
            Arg3 = arg3;
        }

        protected override string ComposeCommand()
        {
            return $"{ActionToken} {Arg1.ToString()}, {Arg2}, {Arg3.ToString()}";
        }

        protected override string ComposeTemplate()
        {
            return $"{ActionToken} [any zero or positive integer], [any string without spaces], [any zero or negative integer]";
        }

        protected override void DoExecute()
        {
            
        }

        protected override void DoParse(IEnumerable<string> arguments)
        {
            Arg1 = int.Parse(arguments.ElementAt(0));
            Arg2 = arguments.ElementAt(1);
            Arg3 = int.Parse(arguments.ElementAt(2));
        }
    }
}
