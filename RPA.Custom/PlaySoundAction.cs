﻿using RPA.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;

namespace RPA.Custom
{
	public enum Sound { Beep, Hand };

	public class PlaySoundAction : RobotAction
	{
        public const string TOKEN = "play-sound";

        public Sound Sound { get; set; }

		public PlaySoundAction()
			: base()
		{
			Description = "Play Sound";
			ActionToken = TOKEN;
			Validator = $"({ActionToken}) (Beep$|Hand$)";
		}

		public PlaySoundAction(Sound sound)
			: this()
		{
			Sound = sound;
		}

		protected override string ComposeCommand()
		{
			return $"{ActionToken} {Sound.ToString()}";
		}

		protected override string ComposeTemplate()
		{
			return $"{ActionToken} [{Sound.Beep.ToString()}|{Sound.Hand.ToString()}]";
		}

		protected override void DoParse(IEnumerable<string> arguments)
		{
			Sound = (Sound)Enum.Parse(typeof(Sound), arguments.First());
		}

		protected override void DoExecute()
		{
			LogInformation($"Playing sound: {Sound}.");
			switch (Sound)
			{
				case Sound.Beep:
					SystemSounds.Beep.Play();
					break;
				case Sound.Hand:
					SystemSounds.Hand.Play();
					break;
				default:
					throw new InvalidOperationException("Unknown sound.");
			}
		}
	}
}
