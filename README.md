# Robotic Process Automation

## Project Skeleton

- `RPA.Abstractions`, contains interfaces and base classes.
- `RPA.Core`, contains robotic action implementations and a proprietary scripting engine.
- `RPA.Server`, WPF application that host a SignalR hub (the server) and an IDE that offers:
    - Robotic action discovery
    - Robotic script composition and persistence inside `Scripts` folder under application execution folder
    - Robotic script execution trigger
    - Hotkey definition
    - Logging, in the UI and rolling file inside `Logs` folder under application execution folder
- `RPA.Agent`, simple console application that implements the SignalR proxy (the client). Offers:
    - Connectivity to the server dual mode communication
    - Hot-key detection and processing
    - Logging, in the console window
    - Robotic script execution hosting
- `RPA.Custom`, contains a custom action (play-sound) implementation, separate from the rest of the project, that only depends on the `RPA.Abstractions` layer.
    - If the module is added (copied) under the `server` execution path, the custom action(s) is/are discovered during the runtime and can be used in script composition.
    - If the module is added (copied) under the `agent` execution path, the custom action(s) is/are discovered during the runtime and can be used in script execution. A script that contains one or more custom actions fails if the corresponsing custom modules are missing. 
- `RPA.Tests`, contains unit/regression tests

## Hotkey Availability

The available hotkeys are: `0-9` & `F1-F12` with all `CTRL` and `ALT` combinations

## Predefined Scripts

Some sample scripts are carried as application resources and they are copied in the `Scripts` folder after building the source code.

**If changes applied during a runtime session, they will be lost after a newer build action.**

These are:

- `mouse-click-in-active-window`, performs a single click in the RPA Server window. It is funny that click targets the very same button causing a chain reaction that executes the same action in an endless loop. Move the mouse so that the button loses focus to stop it.
- `mouse-move-at-500-500`, simple mouse movement at point (500, 500)
- `just-select-notepad`, selects a Notepad window titled `Untitled - Notepad`, if the Notepad application already runs. Error logged otherwise.
- `select-notepad-and-type-text`, just like the previous one but also emitting text.

## Known Issues

Specific key combinations (`CTRL + C` or `CTRL + A`) break the hot-key detections functionality in `RPA.Agent`.